﻿using System;
using System.Collections.Generic;
using System.Linq;

public class EstadisticaDescriptiva
{
    private List<double> datos;

    public EstadisticaDescriptiva(List<double> datos)
    {
        this.datos = datos;
    }

    public double MediaAritmetica() => datos.Average();

    public double Moda() => datos.GroupBy(x => x).OrderByDescending(g => g.Count()).First().Key;

    public double Mediana()
    {
        var listaOrdenada = datos.OrderBy(x => x).ToList();
        int n = listaOrdenada.Count;
        return n % 2 == 0 ? (listaOrdenada[n / 2 - 1] + listaOrdenada[n / 2]) / 2.0 : listaOrdenada[n / 2];
    }

    public Tuple<double, double, double> Cuartiles()
    {
        var listaOrdenada = datos.OrderBy(x => x).ToList();
        int n = listaOrdenada.Count;
        return Tuple.Create(listaOrdenada[(int)(n * 0.25)], listaOrdenada[(int)(n * 0.50)], listaOrdenada[(int)(n * 0.75)]);
    }

    public double Varianza() => datos.Sum(dato => Math.Pow(dato - MediaAritmetica(), 2)) / datos.Count;

    public double DesviacionTipica() => Math.Sqrt(Varianza());

    public double CoeficienteVariacionPearson() => (DesviacionTipica() / MediaAritmetica()) * 100;

    public void Resumen()
    {
        Console.WriteLine("Resumen estadístico:");
        Console.WriteLine($"Media aritmética: {MediaAritmetica()}");
        Console.WriteLine($"Moda: {Moda()}");
        Console.WriteLine($"Mediana: {Mediana()}");
        var cuartiles = Cuartiles();
        Console.WriteLine($"Cuartil 25: {cuartiles.Item1}");
        Console.WriteLine($"Cuartil 50: {cuartiles.Item2}");
        Console.WriteLine($"Cuartil 75: {cuartiles.Item3}");
        Console.WriteLine($"Varianza: {Varianza()}");
        Console.WriteLine($"Desviación típica: {DesviacionTipica()}");
        Console.WriteLine($"Coeficiente de variación de Pearson: {CoeficienteVariacionPearson()}%");
    }
}

class Program
{
    static void Main(string[] args)
    {
        List<double> datos = new List<double> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        EstadisticaDescriptiva estadistica = new EstadisticaDescriptiva(datos);
        estadistica.Resumen();
    }
}
